package com.example.admin.testproject.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.admin.testproject.models.Course;
import com.example.admin.testproject.models.CourseRowMapper;

@Transactional
@Repository

public class CourseDaoImpl implements CourseDao{
//	private String course_name;
//	private String sap_course_id;
//	private String module_abbr;
//	private String module_name;
//	private String prog_name;
	@Autowired
	JdbcTemplate template;
	
	@Override
	public List<Course> getAllCourses() {
		// TODO Auto-generated method stub
		String sql = "select course_name, sap_course_id, module_abbr, module_name, prog_name from schoolofdesign.course";
		RowMapper<Course> rowmapper = new CourseRowMapper();
		List<Course> list = template.query(sql, rowmapper);
		return list;
	}

	@Override
	public Course findById(String sap_course_id) {
		// TODO Auto-generated method stub
		String sql = "select top 1 course_name, sap_course_id, module_abbr, module_name, prog_name from schoolofdesign.course where sap_course_id = ?";
		RowMapper<Course> rm = new BeanPropertyRowMapper<Course>(Course.class);
		Course course = template.queryForObject(sql, rm, sap_course_id);
		return course;
	}

	@Override
	public void addCourse(Course course) {
		// TODO Auto-generated method stub
		String sql = "insert into schoolofdesign.course(course_name, sap_course_id, module_abbr, module_name, prog_name) values (?, ?, ?, ?, ?)";
		template.update(sql, course.getCourse_name(), course.getSap_course_id(), course.getModule_abbr(), course.getModule_name(), course.getProg_name());
	} 

	@Override
	public void updateCourse(Course course) {
		// TODO Auto-generated method stub
		String sql = "update schoolofdesign.course set course_name = ?, module_abbr = ?, module_name = ?, prog_name = ? where sap_course_id = ?";
		template.update(sql, course.getCourse_name(), course.getModule_abbr(), course.getModule_name(), course.getProg_name(), course.getSap_course_id());
	}

	@Override
	public void deleteCourse(String sap_course_id) {
		// TODO Auto-generated method stub
		String sql = "delete from schoolofdesign.course where sap_course_id = ?";
		template.update(sql, sap_course_id);
	}

}
