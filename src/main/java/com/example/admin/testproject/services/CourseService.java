package com.example.admin.testproject.services;

import java.util.List;

import com.example.admin.testproject.models.Course;

public interface CourseService {

	public List<Course> getAllCourses();
	
	public Course findById(String sap_course_id);
	
	public void addCourse(Course course);
	
	public void updateCourse(Course course);
	
	public void deleteCourse(String sap_course_id);
}
