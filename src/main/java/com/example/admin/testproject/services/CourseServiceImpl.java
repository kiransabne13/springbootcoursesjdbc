package com.example.admin.testproject.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.admin.testproject.dao.CourseDaoImpl;
import com.example.admin.testproject.models.Course;

@Service
public class CourseServiceImpl implements CourseService{

	@Autowired
	CourseDaoImpl courseDaoImpl;
	
	@Override
	public List<Course> getAllCourses() {
		// TODO Auto-generated method stub
		return courseDaoImpl.getAllCourses();
	}

	@Override
	public Course findById(String sap_course_id) {
		// TODO Auto-generated method stub
		return courseDaoImpl.findById(sap_course_id);
	}

	@Override
	public void addCourse(Course course) {
		// TODO Auto-generated method stub
		courseDaoImpl.addCourse(course);
	}

	@Override
	public void updateCourse(Course course) {
		// TODO Auto-generated method stub
		courseDaoImpl.updateCourse(course);
	}

	@Override
	public void deleteCourse(String sap_course_id) {
		// TODO Auto-generated method stub
		courseDaoImpl.deleteCourse(sap_course_id);
	}

	
}
