package com.example.admin.testproject.models;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CourseRowMapper implements RowMapper<Course>{
//	private String course_name;
//	private String sap_course_id;
//	private String module_abbr;
//	private String module_name;
//	private String prog_name;
	@Override
	public Course mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Course course = new Course();
		course.setCourse_name(rs.getString("course_name"));
		course.setSap_course_id(rs.getString("sap_course_id"));
		course.setModule_abbr(rs.getString("module_abbr"));
		course.setModule_name(rs.getString("module_name"));
		course.setProg_name(rs.getString("prog_name"));
		return course;
	}

}
