package com.example.admin.testproject.models;

public class Course {

//	SELECT TOP (1000) [course_name]
//		      ,[sap_course_id]
//		      ,[module_abbr]
//		      ,[module_name]
//		      ,[prog_name]
//		      ,[subject_code]
//		      ,[subject_abbr]
//		      ,[session]
//		      ,[registered]
//		      ,[prog_code]
//		      ,[year]
//		      ,[passingPercentage]
//		      ,[students_booked_to_event]
//		      ,[ica_tee_split]
//		      ,[acadYear]
//		      ,[active]
//		  FROM [SchoolOfDesign].[schoolofdesign].[course]
	
	private String course_name;
	private String sap_course_id;
	private String module_abbr;
	private String module_name;
	private String prog_name;
	
	public String getCourse_name() {
		return course_name;
	}
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
	public String getSap_course_id() {
		return sap_course_id;
	}
	public void setSap_course_id(String sap_course_id) {
		this.sap_course_id = sap_course_id;
	}
	public String getModule_abbr() {
		return module_abbr;
	}
	public void setModule_abbr(String module_abbr) {
		this.module_abbr = module_abbr;
	}
	public String getModule_name() {
		return module_name;
	}
	public void setModule_name(String module_name) {
		this.module_name = module_name;
	}
	public String getProg_name() {
		return prog_name;
	}
	public void setProg_name(String prog_name) {
		this.prog_name = prog_name;
	}
	@Override
	public String toString() {
		return "Course [course_name=" + course_name + ", sap_course_id="
				+ sap_course_id + ", module_abbr=" + module_abbr
				+ ", module_name=" + module_name + ", prog_name=" + prog_name
				+ "]";
	}
	public Course(String course_name, String sap_course_id, String module_abbr,
			String module_name, String prog_name) {
		super();
		this.course_name = course_name;
		this.sap_course_id = sap_course_id;
		this.module_abbr = module_abbr;
		this.module_name = module_name;
		this.prog_name = prog_name;
	}

	public Course(){
		
	}
}
