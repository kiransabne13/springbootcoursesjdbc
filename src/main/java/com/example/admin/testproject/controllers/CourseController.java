package com.example.admin.testproject.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.ModelAndView;

import com.example.admin.testproject.models.Course;
import com.example.admin.testproject.services.CourseServiceImpl;

@Controller

public class CourseController {

	@Autowired
	CourseServiceImpl courseServiceImpl;
	
	 @RequestMapping(value= {"/", "/list"}, method=RequestMethod.GET)
	public ModelAndView getAllCourses(){
		 ModelAndView model = new ModelAndView();
		 List<Course> list = courseServiceImpl.getAllCourses();
		 
		 model.addObject("course_list", list);
		 model.setViewName("new_course_list");
		 return model;
	 }
	 
	 @RequestMapping(value = "/login", method=RequestMethod.GET)
		public ModelAndView getAll1(){
		 ModelAndView model = new ModelAndView();
//		 List<Course> list = courseServiceImpl.getAllCourses();
//		 
//		 model.addObject("course_list", list);
		 model.setViewName("login");
		 return model;
	 }
	 
	 
	 @RequestMapping(value= {"listone"}, method=RequestMethod.GET)
	public ModelAndView getAll(){
		 ModelAndView model = new ModelAndView();
		 List<Course> list = courseServiceImpl.getAllCourses();
		 
		 model.addObject("course_list", list);
		 model.setViewName("course_list.html");
		 return model;
	 }
	
	 
	 @RequestMapping(value="/update/{sap_course_id}", method=RequestMethod.GET)
	 public ModelAndView editCourse(@PathVariable String sap_course_id){
		 ModelAndView model = new ModelAndView();
		 Course course = courseServiceImpl.findById(sap_course_id);
		 model.addObject("courseForm", course);
		 model.setViewName("course_form");
		 
		 return model;
	 }
	 
	 @RequestMapping(value="/add", method=RequestMethod.GET)
	 public ModelAndView addCourse(){
		 ModelAndView model = new ModelAndView();
		 Course course = new Course();
		 model.addObject("courseForm", course);
		 model.setViewName("course_form");
		 return model;
		 
	 }
	 
	 @RequestMapping(value="/save", method=RequestMethod.POST)
	 public ModelAndView saveOrUpdateCourse(@ModelAttribute("courseForm") Course course){
		 if (course.getSap_course_id() != null){
			 courseServiceImpl.updateCourse(course);
		 } else {
			 courseServiceImpl.addCourse(course);
		 }
		 
		 return new ModelAndView("redirect:/list");
	 }
	 
	 @RequestMapping(value="/delete/{sap_course_id}", method=RequestMethod.GET)
	 public ModelAndView deleteCourse(@PathVariable("sap_course_id") String sap_course_id){
		 courseServiceImpl.deleteCourse(sap_course_id);
		 return new ModelAndView("redirect:/list");
	 }
}
