<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <title>Courses</title>
 <link href="http://localhost:8080/webjars/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
 <script src="http://localhost:8080/webjars/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
 <script src="http://localhost:8080/webjars/jquery/3.0.0/js/jquery.min.js" ></script>
</head>
<body>
 <div class="container">
  <spring:url value="/save" var="saveURL" />
  <h2>Employee</h2>
  <form:form modelAttribute="courseForm" method="post" action="${saveURL }" cssClass="form">
   <form:hidden path="sap_course_id"/>
   <div class="form-group">
    <label for="courseName">Course Name</label>
    <form:input path="course_name" cssClass="form-control" id="courseName" />
   </div>
   <div class="form-group">
    <label for="sap_course_id">Sap Course Id</label>
    <form:input path="sap_course_id" cssClass="form-control" id="sap_course_id" />
   </div>
   <div class="form-group">
    <label for="module_abbr">Abbr</label>
    <form:input path="module_abbr" cssClass="form-control" id="module_abbr" />
   </div>
   <div class="form-group">
    <label for="module_name">Name</label>
    <form:input path="module_name" cssClass="form-control" id="module_name" />
   </div>
   <div class="form-group">
    <label for="prog_name">Prog Name</label>
    <form:input path="prog_name" cssClass="form-control" id="prog_name" />
   </div>
   <button type="submit" class="btn btn-primary">Save</button>
  </form:form>
 </div>
</body>
</html>