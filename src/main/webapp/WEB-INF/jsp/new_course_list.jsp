<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <title>Course List</title>
 <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
 <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet"/> 
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.js" ></script>
<!--  <script src="../webjars/jquery/3.0.0/js/jquery.min.js" ></script>
 -->
</head>
<body>
 <div class="container">
  <h2>Course List</h2>
 <%--  <table class="table table-striped">
   <thead>
    <tr>
     <th scope="row">Course Name</th>
     <th scope="row">Sap Course Id</th>
     <th scope="row">Module Abbr</th>
     <th scope="row">Module Name</th>
     <th scope="row">Prog Name</th>
    </tr>
   </thead>
   <tbody>
    <c:forEach items="${course_list }" var="course" >
     <tr>
      <td>${course.course_name }</td>
      <td>${course.sap_course_id }</td>
      <td>${course.module_abbr }</td>
      <td>${course.module_name }</td>
      <td>${course.prog_name }</td>
      
      <td>
       <spring:url value="/update/${course.sap_course_id }" var="updateURL" />
       <a class="btn btn-primary" href="${updateURL }" role="button">Update</a>
      </td>
      <td>
       <spring:url value="/delete/${course.sap_course_id }" var="deleteURL" />
       <a class="btn btn-primary" href="${deleteURL }" role="button">Delete</a>
      </td>
     </tr>
    </c:forEach>
   </tbody>
  </table> --%>
  
  <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Course Name</th>
                <th>Sap Course Id</th>
                <th>Module Abbr</th>
                <th>Module Name</th>
                <th>Prog Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
   <tbody>
    <c:forEach items="${course_list }" var="course" >
     <tr>
      <td>${course.course_name }</td>
      <td>${course.sap_course_id }</td>
      <td>${course.module_abbr }</td>
      <td>${course.module_name }</td>
      <td>${course.prog_name }</td>
      <td>
       <spring:url value="/update/${course.sap_course_id }" var="updateURL" />
       <a class="btn btn-primary" href="${updateURL }" role="button">Update</a>
      </td>
      <td>
       <spring:url value="/delete/${course.sap_course_id }" var="deleteURL" />
       <a class="btn btn-primary" href="${deleteURL }" role="button">Delete</a>
      </td>

     </tr>
    </c:forEach>
   </tbody>
        <tfoot>
            <tr>
                <th>Course Name</th>
                <th>Sap Course Id</th>
                <th>Module Abbr</th>
                <th>Module Name</th>
                <th>Prog Name</th>
            </tr>
        </tfoot>
    </table>
  
  
  <spring:url value="/add" var="addURL" />
  <a class="btn btn-primary" href="${addURL }" role="button">Add New Course</a>
 </div>
 
       <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
          <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
          <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
   <script>$(document).ready(function() {
	    $('#example').DataTable();
   } );</script>
        
</body>
</html>